package com.nttdata.springcloudrest.controller;

import com.nttdata.springcloudrest.entity.Movie;
import com.nttdata.springcloudrest.service.IMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class MovieController {

    @Value("${ruta.servicio.externo1}")
    private String propiedad1;

    @Value("${ruta.servicio.externo2}")
    private String propiedad2;

    @Autowired
    private IMovieService movieService;

    @GetMapping("/listar")
    List<Movie> listarMovie() {
        return movieService.buscarMovies();
    }

    @GetMapping("/buscar/nombre")
    List<Movie> findAllMoviesByName(@RequestParam("nombre") String name) {
        return movieService.buscarMoviesPorName(name);
    }

    @GetMapping("/buscar/{id}")
    Movie buscarPorId(@PathVariable("id") Long id) throws IllegalAccessException {

        System.out.println("propiedad1 = " + propiedad1);
        System.out.println("propiedad2 = " + propiedad2);
        if (id == 10) {
            throw new IllegalAccessException("Error por id 10");
        }
        return movieService.buscarPorId(id);
    }

    @PostMapping("/crear")
    Movie crear(@RequestBody Movie movie) {
        return movieService.crearMovie(movie);
    }

    @PutMapping("/actualizar/{id}")
    Movie actualizar(@RequestParam Map<String, String> parameters, @RequestHeader HttpHeaders headers, @PathVariable("id") Long id, @RequestBody Movie movie) {

        System.out.println("Cabeceras: " + headers.toString());
        System.out.println("Parametros: " + parameters.toString());

        return movieService.actualizarMovie(id, movie);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return movieService.eliminarMovie(id);
    }


}
