package com.nttdata.springcloudrest.service;

import com.nttdata.springcloudrest.entity.Movie;

import java.util.List;

public interface IMovieService {
    List<Movie> buscarMovies();

    List<Movie> buscarMoviesPorName(String razonSocial);

    Movie crearMovie(Movie movie);

    Movie actualizarMovie(Long id, Movie movie);

    String eliminarMovie(Long id);

    Movie buscarPorId(Long id);
}
