package com.nttdata.springcloudrest.service;

import com.nttdata.springcloudrest.entity.Movie;
import com.nttdata.springcloudrest.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements IMovieService {

    @Value("${server.port}")
    private int port;

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public List<Movie> buscarMovies() {

        return movieRepository.findAll();
    }

    @Override
    public Movie buscarPorId(Long id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            movie.get().setPort(port);
            return movie.get();
        }
        return null;
    }

    @Override
    public List<Movie> buscarMoviesPorName(String name) {
        return movieRepository.findByNameContaining(name);
    }

    @Override
    public Movie crearMovie(Movie movie) {

        return movieRepository.save(movie);
    }

    @Override
    public Movie actualizarMovie(Long id, Movie movie) {
        movie.setId(id);
        return movieRepository.save(movie);
    }


    @Override
    public String eliminarMovie(Long id) {
        try {
            movieRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
