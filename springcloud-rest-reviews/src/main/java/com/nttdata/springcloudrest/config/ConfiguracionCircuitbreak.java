package com.nttdata.springcloudrest.config;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class ConfiguracionCircuitbreak {
    @Bean
    public Customizer<Resilience4JCircuitBreakerFactory> defaultCustomizer() {
        return factory -> factory.configureDefault(id -> {
            return new Resilience4JConfigBuilder(id).circuitBreakerConfig(
                    CircuitBreakerConfig.custom()
                            .slidingWindowSize(20)
                            .failureRateThreshold(50L)
                            .waitDurationInOpenState(Duration.ofSeconds(20))
                            .permittedNumberOfCallsInHalfOpenState(5)
                            .build()
            ).timeLimiterConfig(TimeLimiterConfig.ofDefaults()).build();
        });
    }
}
