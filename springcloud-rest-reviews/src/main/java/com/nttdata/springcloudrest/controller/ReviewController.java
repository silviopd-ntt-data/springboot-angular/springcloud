package com.nttdata.springcloudrest.controller;

import com.nttdata.springcloudrest.entity.Review;
import com.nttdata.springcloudrest.service.IReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@RequestMapping("/review")
@RestController
public class ReviewController {

    @Autowired
    private IReviewService reviewService;

    @GetMapping("/listar")
    List<Review> listarReview() {
        return reviewService.buscarReviews();
    }

    @GetMapping("/buscar/{id}")
    Review buscarPorId(@PathVariable("id") Long id) {
        return reviewService.buscarPorId(id);
    }

    @PostMapping("/crear")
    Review crear(@RequestBody Review review) {
        return reviewService.crearReview(review);
    }

    @PutMapping("/actualizar/{id}")
    Review actualizar(@RequestHeader HttpHeaders headers, @PathVariable("id") Long id, @RequestBody Review review) {

        System.out.println(headers.toString());
        return reviewService.actualizarReview(id, review);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return reviewService.eliminarReview(id);
    }


}
