package com.nttdata.springcloudrest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String comment;

    private String reviewers;

    private Date createAt;

    @ManyToOne
    @JoinColumn(name = "id_movie")
    @JsonIgnoreProperties("reviewList")
    private Movie movie;


}
