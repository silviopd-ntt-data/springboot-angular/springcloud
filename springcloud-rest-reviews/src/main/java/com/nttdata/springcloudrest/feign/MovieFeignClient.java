package com.nttdata.springcloudrest.feign;

import com.nttdata.springcloudrest.entity.Movie;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "servicio-movie")
public interface MovieFeignClient {
    //    @GetMapping("/movie/buscar/{id}")
    @GetMapping("/buscar/{id}")
    Movie buscarPorId(@PathVariable("id") Long id);
}
