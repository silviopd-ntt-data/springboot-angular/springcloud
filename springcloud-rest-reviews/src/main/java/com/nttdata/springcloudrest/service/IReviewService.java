package com.nttdata.springcloudrest.service;

import com.nttdata.springcloudrest.entity.Review;

import java.util.List;

public interface IReviewService {
    List<Review> buscarReviews();

    Review crearReview(Review review);

    Review actualizarReview(Long id, Review review);

    String eliminarReview(Long id);

    Review buscarPorId(Long id);
}
