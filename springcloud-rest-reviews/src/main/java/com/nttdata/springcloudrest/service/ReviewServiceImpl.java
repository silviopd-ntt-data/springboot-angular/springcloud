package com.nttdata.springcloudrest.service;

import com.nttdata.springcloudrest.entity.Movie;
import com.nttdata.springcloudrest.entity.Review;
import com.nttdata.springcloudrest.feign.MovieFeignClient;
import com.nttdata.springcloudrest.repository.ReviewRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Service
public class ReviewServiceImpl implements IReviewService {

    @Autowired
    private MovieFeignClient feignClient;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    @Override
    public List<Review> buscarReviews() {

        return reviewRepository.findAll();
    }

    @Override
    public Review buscarPorId(Long id) {
        return reviewRepository.findById(id).orElse(null);
    }


    @Override
    @CircuitBreaker(name = "moviebuscar", fallbackMethod = "metodoAlternativo")
    public Review crearReview(Review review) {

//        Movie movie = restTemplate.getForObject("http://localhost:8002/movie/buscar/" + review.getMovie().getId(), Movie.class);
//        Movie movie = restTemplate.getForObject("http://servicio-movie/movie/buscar/" + review.getMovie().getId(), Movie.class);


//        return circuitBreakerFactory.create("moviebuscar").run(() -> {
//            Movie movie = feignClient.buscarPorId(review.getMovie().getId());
//            if (Objects.nonNull(movie)) {
//                Review reviewCreated = reviewRepository.save(review);
//                reviewCreated.setMovie(movie);
//                return reviewCreated;
//            }
//            return null;
//        }, ex -> {
//            return metodoAlternativo(ex);
//        });


        Movie movie = feignClient.buscarPorId(review.getMovie().getId());
        if (Objects.nonNull(movie)) {
            Review reviewCreated = reviewRepository.save(review);
            reviewCreated.setMovie(movie);
            return reviewCreated;
        }
        return null;
    }


    private Review metodoAlternativo(Throwable ex) {
        System.out.println(ex.getMessage());
        Review reviewAlternative = new Review();
        reviewAlternative.setId(0L);
        reviewAlternative.setComment("comentario por defecto");
        reviewAlternative.setReviewers("Reviewer Default");
        return reviewAlternative;
    }


    @Override
    public Review actualizarReview(Long id, Review review) {
        review.setId(id);
        return reviewRepository.save(review);
    }


    @Override
    public String eliminarReview(Long id) {
        try {
            reviewRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
